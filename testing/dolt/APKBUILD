# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=dolt
pkgver=1.39.2
pkgrel=0
pkgdesc="Dolt – It's Git for Data"
url="https://www.dolthub.com"
arch="all !x86 !armhf !armv7" # fails on 32-bit
license="Apache-2.0"
options="!check chmod-clean net"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/dolthub/dolt/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgver/go"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	mkdir -p build
	go build \
		-mod=readonly \
		-ldflags "-extldflags \"$LDFLAGS\"" \
		-o build \
		./cmd/...
}

package() {
	install -Dm755 build/dolt "$pkgdir"/usr/bin/dolt
}

sha512sums="
3e6aac7aa822e10455ee8282b77803d30597e5fcd712ec72525284ec275b9412870500d092c125f8f02f5504a21adceead08d0caf3fb029b18927eaab653e7bf  dolt-1.39.2.tar.gz
"
